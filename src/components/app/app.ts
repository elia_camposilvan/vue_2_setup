import Vue from "vue";
import {Component} from "vue-property-decorator";
import "./app.scss";
const template = require("./app.vue");

@Component({
    template: template
})
export class AppComponent extends Vue {

  private totalDisks = 10;

  private repo = "https://github.com/ducksoupdev/vue-webpack-typescript";

}

Vue.component("cmApp", AppComponent);

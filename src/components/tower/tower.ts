import Vue from "vue";
import {Component, Prop} from "vue-property-decorator";
import "./tower.scss";
const template = require("./tower.vue");

@Component({
  template: template,
})
export class HanoiTowerComponent extends Vue {

  @Prop
  private diskNumber: number;

  private rods = [[], [], []];

  private created(): void {
    this.rods[0] = this.generateDisks(this.diskNumber);
  }

  private generateDisks(diskNumber: number): {width: number}[] {
    const disks = [];
    for (let i = 0; i < diskNumber; i++) {
      disks.push({ width: i + 1 });
    }
    return disks;
  }

}

Vue.component("CmHanoiTower", HanoiTowerComponent);

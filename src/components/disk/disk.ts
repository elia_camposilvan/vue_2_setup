import Vue from "vue";
import "./disk.scss";
import {Component, Prop} from "vue-property-decorator";
const template = require("./disk.vue");

@Component({
    template: template
})
export class HanoiDiskComponent extends Vue {

  @Prop
  private width: number;

  private getWidthStyle(): string {
    return (this.width * 20) + 30 + "px";
  }

}

Vue.component("CmHanoiDisk", HanoiDiskComponent);
